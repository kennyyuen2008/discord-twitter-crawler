# Discord Twitter Crawler

Discord Twitter Crawler is a discord bot for streaming tweets post by specified Twitter user.

## Config

This project use dot env. Fill in your discord bot token, twitter api tokens in .env.

```
DISCORD_TOKEN={YOUR_DISCORD_TOKEN}
TWITTER_API_KEY={YOUR_TWITTER_API_KEY}
TWITTER_API_SECRET_KEY={YOUR_TWITTER_API_SECRET}
TWITTER_ACCESS_TOKEN={YOUR_TWITTER_ACCESS_TOKEN}
TWITTER_ACCESS_TOKEN_SECRET={YOUR_TWITTER_ACCESS_TOKEN}
```

## Installation

To run this project, install dependency

```bash
$ npm install
$ npm start
```

## Usage

Modify the following line to suit your needs.

```javascript
// SETTING DISCORD BOT STATUS
discord.user.setActivity("Twitter", { type: "PLAYING" });

// TWITTER ID TO FOLLOW USING TWITTER STREAM API ETC
const twitterID = "783214";
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
