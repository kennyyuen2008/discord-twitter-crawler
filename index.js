require("dotenv").config();

const Discord = require("discord.js");
const discord = new Discord.Client();

// LOGIN DISCORD
discord.login(process.env.DISCORD_TOKEN);

// EVENT LISTENER, CALLBACK WHEN DISCORD CLIENT READY
discord.on("ready", () => {
   console.log("DISCORD CLIENT READY");

   // SETTING DISCORD BOT STATUS
   discord.user.setActivity("Twitter", { type: "PLAYING" });

   const Twit = require("twit");

   // CONNECT TO TWITTER API
   const twitter = new Twit({
      consumer_key: process.env.TWITTER_API_KEY,
      consumer_secret: process.env.TWITTER_API_SECRET_KEY,
      access_token: process.env.TWITTER_ACCESS_TOKEN,
      access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
   });

   // START A TWITTER STREAM

   // GET TWEETS POST BY TWITTER ID
   // TWITTER ID CAN BE OBTAIN FROM http://gettwitterid.com/ OR https://tweeterid.com/ ETC
   const twitterID = "783214";
   let stream = twitter.stream("statuses/filter", { follow: twitterID });

   // LISTEN TO TWEET STREAM
   stream.on("tweet", (tweet) => {
      // FILTER OUT RETWEET
      if (tweet.hasOwnProperty("retweeted_status")) return;
      // FILTER OUT REPLY
      if (tweet.in_reply_to_user_id) return;
      // LOG TWEET
      console.log(`Received tweet: ${JSON.stringify(tweet)}`);

      // LOOP THROUGH EACH CHANNEL
      discord.channels.cache.each((channel) => {
         // FILTER OUT NON-TEXT CHANNEL
         if (channel.type !== "text") return;
         // CHECK FOR PERMISSION TO SEND MESSAGES IN THE CHANNEL
         if (!channel.permissionsFor(discord.user).has("SEND_MESSAGES")) return;
         // FILTER OUT CHANNEL NOT NAME AS 'general'
         if (channel.name !== "general") return;

         try {
            // SEND A MESSAGE
            channel.send({
               // SEND AS A EMBED MESSAGE
               embed: {
                  // SETTING EMBED COLOR
                  color: 0x0099ff,
                  author: {
                     name: tweet.user.name,
                     icon_url: tweet.user.profile_image_url_https,
                     url: `https://twitter.com/${tweet.user.screen_name}/status/${tweet.id_str}`,
                  },
                  thumbnail: {
                     url: tweet.user.profile_image_url_https,
                  },
                  // TWITTER API WILL TRUNCATE TWEET TEXT IF TEXT LENGTH > 140 CHARACTERS
                  // GET EXTENDED TWEET IF TWEET IS TRUNCATED
                  description: tweet.truncated
                     ? tweet.extended_tweet && tweet.extended_tweet.full_text
                     : tweet.text,
                  timestamp: new Date(),
               },
            });
            // HANDLE QUOTED TWEET
            if (tweet.hasOwnProperty("quoted_status")) {
               channel.send({
                  embed: {
                     title: "Quoted Tweet",
                     author: {
                        name: tweet.quoted_status.user.name,
                        icon_url:
                           tweet.quoted_status.user.profile_image_url_https,
                        url: `https://twitter.com/${tweet.user.screen_name}/status/${tweet.quoted_status.id_str}`,
                     },
                     description: tweet.quoted_status.text,
                  },
               });
            }
         } catch (error) {
            console.log(error);
         }
      });
   });
});
